import { Component, OnInit } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { Basket } from 'src/app/models/basket'
import { Product } from 'src/app/models/product'
import { AuthenticationService } from 'src/app/services/authentication.service'
import { CRMService } from 'src/app/services/crm.service'
import { ProductService } from 'src/app/services/product.service'
import { ShopService } from 'src/app/services/shop.service'
import { RegisterDialogComponent } from '../register-dialog/register.dialog.component'

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.sass']
})
export class BasketComponent implements OnInit {

  basket: Basket | undefined

  constructor(
    readonly productService: ProductService,
    readonly crmService: CRMService,
    readonly shopService: ShopService,
    readonly authenticationService: AuthenticationService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.basket = this.crmService.basket
    console.log(this.basket)
  }

  removeFromBasket(product: Product) {
    // const alreadyExistingProductIndex = this.basket!.products.findIndex(p => p.id == product.id)
    // if (alreadyExistingProductIndex > -1) {
    //   if (this.basket!.products[alreadyExistingProductIndex]!.quantity > 1) {
    //     this.basket!.products[alreadyExistingProductIndex]!.quantity -= 1
    //   } else {
    //     this.basket!.products.splice(alreadyExistingProductIndex, 1)
    //   }
    // }
    this.crmService.removeProductFromBasket(product)
  }

  addToBasket(product: Product) {
    // this.basket?.products.find(p => p.quantity += 1)
    this.crmService.addProductToBasket(product)
  }

  purchaseAll() {
    this.basket?.products.forEach(p => {
      this.crmService.createOrder(p.id, p.price)
    });
    if (this.authenticationService.userIsLogged()) {
      this.shopService.createCheckoutSession(this.basket!.products)
    } else {
      this.openDialog()
    }
  }

  /**
* Open the dialog modal and return created slides after close
*/
  public openDialog(): void {
    this.dialog.open(RegisterDialogComponent, {
      hasBackdrop: true,
      backdropClass: "dialog-backdrop",
      autoFocus: true,
    })
  }

}
