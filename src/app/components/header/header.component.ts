import { Component, OnInit } from "@angular/core"
import { FormControl, FormGroup, Validators } from "@angular/forms"
import { MatDialog } from "@angular/material/dialog"
import { Right } from "src/app/models/user/user"
import { AuthenticationService } from "src/app/services/authentication.service"
import { RegisterDialogComponent } from "../register-dialog/register.dialog.component"

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.sass"],
})
export class HeaderComponent implements OnInit {

  constructor(public dialog: MatDialog,
    public authenticationService: AuthenticationService
  ) { }

  IsLoginDisplayed: boolean = false
  IsRegisterDisplayed: boolean = false
  IsGuestUsernameEdit: boolean = false
  panelOpenState = false

  guestGroup = new FormGroup({
    username: new FormControl(this.authenticationService.getUser()?.username, [Validators.required, Validators.min(3), Validators.max(20), Validators.pattern(/([a-zA-Z0-9])\w+/)]),
  })

  ngOnInit(): void { }

  userConnectedIsAdmin(): boolean {
    return this.authenticationService.getUser()?.right === Right.ADMIN
  }

  toggleRegister() {
    this.IsLoginDisplayed = false
    this.openDialog()
  }

  public logout() {
    this.authenticationService.logout()
  }

  /**
 * Open the dialog modal and return created slides after close
 */
  public openDialog(): void {
    const dialogRef = this.dialog.open(RegisterDialogComponent, {
      hasBackdrop: true,
      backdropClass: "dialog-backdrop",
      autoFocus: true,
    })
    dialogRef.afterClosed().subscribe(() => {
      this.IsRegisterDisplayed = false
    })
  }
}
