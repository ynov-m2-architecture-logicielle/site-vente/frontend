import { Component, Inject } from "@angular/core"
import { FormControl, FormGroup, Validators } from "@angular/forms"
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog"
import { CustomerCRM } from "src/app/models/user/customerCRM"
import { FrontendUser } from "src/app/models/user/frontend-user"
import { AuthenticationService } from "src/app/services/authentication.service"
import { CRMService } from "src/app/services/crm.service"

/**
 * This component is a modal that display a creation slide form with file upload
 */
@Component({
  selector: "app-register-dialog",
  templateUrl: "./register.dialog.component.html",
  styleUrls: ["./register.dialog.component.sass"],
})
export class RegisterDialogComponent {

  registerGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
    username: new FormControl(null, [Validators.required, Validators.min(3), Validators.max(20), Validators.pattern(/([a-zA-Z0-9])\w+/)]),
    address: new FormControl(null, [Validators.required]),
    phoneNumber: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required, Validators.min(7), Validators.max(30)]),
    confirmPassword: new FormControl(null, [Validators.required, Validators.min(7), Validators.max(30)]),
  })
  //, Validators.min(10), Validators.max(10), Validators.pattern(/([0-9])\w+/)]

  loginGroup = new FormGroup({
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, [Validators.required]),
  })

  registerFormError: string | null = null
  loginFormError: string | null = null

  translated = false

  constructor(
    public dialogRef: MatDialogRef<RegisterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authenticationService: AuthenticationService, private crmService: CRMService) { }


  public onNoClick(): void {
    this.dialogRef.close()
  }

  register() {
    this.registerFormError = null
    if (this.registerGroup.invalid) {
      this.registerFormError = "Tous les champs ne sont pas valides."
      return
    }

    let username: string = this.registerGroup.get("username")?.value
    let address: string = this.registerGroup.get("address")?.value
    let phoneNumber: string = this.registerGroup.get("phoneNumber")?.value
    let email: string = this.registerGroup.get("email")?.value
    let password: string = this.registerGroup.get("password")?.value
    let confirmPassword: string = this.registerGroup.get("confirmPassword")?.value
    const customerCRM: CustomerCRM = new CustomerCRM({
      name: username,
      email: email,
      address: address,
      phoneNumber: phoneNumber
    })
    if (password !== confirmPassword) {
      this.registerFormError = "Les mots de passe ne correspondent pas."
      return
    }

    this.authenticationService.register(username, email, password).subscribe({
      next: (response) => {
        this.authenticationService.saveUserInLocalStorage(new FrontendUser({ ...response }))
        this.crmService.createCustomer(customerCRM)

        this.onNoClick()
      },
      error: (err) => {
        console.log(err)
        this.registerFormError = "Cette adresse email est déjà utilisée."
      },
    })
  }

  login() {
    this.loginFormError = null
    if (this.loginGroup.invalid) {
      this.loginFormError = "tous les champs ne sont pas valides."
      return
    }

    let email: string = this.loginGroup.get("email")?.value
    let password: string = this.loginGroup.get("password")?.value

    this.authenticationService.login(email, password).subscribe({
      next: (response) => {
        this.authenticationService.saveUserInLocalStorage(new FrontendUser({ ...response }))
        this.onNoClick()
      },
      error: (err) => {
        console.log(err)
        this.loginFormError = err.statusText
      },
    })
  }

}
