import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Product } from 'src/app/models/product'
import { Tag, TagCategory } from 'src/app/models/tag'
import { ProductService } from 'src/app/services/product.service'

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.sass']
})
export class CatalogComponent implements OnInit {

  itemTypes: Tag[] = []

  currentItemType: Tag | undefined

  products: Product[] = []

  constructor(readonly productService: ProductService, readonly route: ActivatedRoute) { }

  ngOnInit() {
    this.currentItemType = this.route.snapshot.params['itemType']
    this.itemTypes = this.productService.tags.filter(t => t.category == TagCategory.TYPE)
    this.products = this.productService.products
  }

  getItemTypeRoute(itemType: Tag): string {
    return itemType.name.toLocaleLowerCase().replace(' ', '-')
  }

  getProductRoute(product: Product): string {
    return 'p/' + product.id
  }

}
