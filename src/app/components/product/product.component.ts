import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Product } from 'src/app/models/product'
import { CRMService } from 'src/app/services/crm.service'
import { ProductService } from 'src/app/services/product.service'
import { ShopService } from 'src/app/services/shop.service'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent implements OnInit {

  product: Product | undefined

  constructor(
    readonly productService: ProductService,
    readonly CRMService: CRMService,
    readonly shopService: ShopService,
    readonly route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const productId = this.route.snapshot.params['productId']
    this.product = this.productService.products.find(p => p.id == productId)
    if (this.product == undefined) {
      this.router.navigate(["c"])
    }
  }

  purchase() {
    this.shopService.createCheckoutSession([this.product!])
  }

  addToBasket(product: Product) {
    this.CRMService.addProductToBasket(product)
  }

}
