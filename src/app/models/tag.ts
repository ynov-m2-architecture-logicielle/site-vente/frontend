export enum TagCategory {
    COLOR,
    TYPE,
    SEASON
}

export class Tag {

    public id: string
    public name: string
    public category: TagCategory
    public imageSrc: string

    constructor(tag: Tag) {
        this.id = tag.id
        this.name = tag.name
        this.category = tag.category
        this.imageSrc = tag.imageSrc
    }

}