export enum Right {
    DEFAULT = "default",
    ADMIN = "admin",
}

export interface User {

    id: string
    right?: Right
    username: string
}