import { Right, User } from "./user";

/**
 * User class for frontend purpose
 */
export class FrontendUser implements User {

    public id: string
    public token: string
    public right: Right
    public email: string
    public username: string

    constructor(user: FrontendUser) {
        this.id = user.id
        this.token = user.token
        this.right = user.right
        this.email = user.email
        this.username = user.username
    }
}