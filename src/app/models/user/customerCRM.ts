export class CustomerCRM {

    public id?: string;
    public name?: string;
    public email?: string;
    public address?: string;
    public phoneNumber?: string;

    constructor(user: Partial<CustomerCRM> = {}) {
        this.id = user.id;
        this.name = user.name;
        this.email = user.email;
        this.address = user.address;
        this.phoneNumber = user.phoneNumber;
    }
}
