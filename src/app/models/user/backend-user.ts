import { Right, User } from './user';

/**
 * User class for backend purpose
 */
export class BackendUser implements User {

    public id: string
    public right: Right
    public username: string
    public email: string
    public password?: string

    constructor(user: BackendUser) {
        this.id = user.id
        this.right = user.right
        this.username = user.username
        this.email = user.email
        this.password = user.password
    }

}