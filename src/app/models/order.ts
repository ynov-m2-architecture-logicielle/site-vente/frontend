export class OrderCRM {

    public id?: string
    public customerId?: string
    public productId: string
    public price: number

    constructor(idproduct: string, price: number) {
        this.productId = idproduct
        this.price = price
    }
}