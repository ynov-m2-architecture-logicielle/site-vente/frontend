import { Tag } from "./tag"

export class Product {

    public id: string
    public name: string
    public price: number
    public description: string
    public tags: Tag[]
    public imageSrc: string
    public quantity: number
    public relatedProducts: Product[]

    constructor(product: Product) {
        this.id = product.id
        this.name = product.name
        this.price = product.price
        this.description = product.description
        this.tags = product.tags
        this.imageSrc = product.imageSrc
        this.quantity = product.quantity
        this.relatedProducts = product.relatedProducts
    }

}