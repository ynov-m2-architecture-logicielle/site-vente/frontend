import { Product } from "./product"

export class Basket {

    public id: string
    public products: Product[]
    public customerId: string
    public productsId?: string

    constructor(id: string, customerId: string, products: Product[], productsId?: string) {
        this.id = id
        this.products = products
        this.customerId = customerId
        this.productsId = ""
        products.forEach(product => {
            this.productsId += product.id + ";"
        });
    }

    public majBasket() {
        this.productsId = "";
        this.products.forEach(product => {
            this.productsId += product.id + ";"
        });
    }
}