import { Injectable } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { environment } from "src/environments/environment"
import { Observable } from "rxjs"
import { Basket } from "../models/basket"
import { AuthenticationService } from "./authentication.service"
import { Product } from "../models/product"
import { FrontendUser } from "../models/user/frontend-user"
import { CustomerCRM } from "../models/user/customerCRM"
import { OrderCRM } from "../models/order"

@Injectable({
    providedIn: "root"
})
export class CRMService {
    url = environment.CRM_BACKEND_URL

    basket: Basket | undefined
    currentUser: FrontendUser | undefined

    constructor(private http: HttpClient, readonly authenticationService: AuthenticationService) {
        this.currentUser = AuthenticationService.currentUser
        this.getBasket()
    }

    private getBasket() {
        if (this.authenticationService.userIsLogged()) {
            this.http.get(this.url + "customer/" + this.currentUser!.id + "/basket").subscribe({
                next: (response: any) => {
                    this.basket = response
                    this.saveLocaleBasket()
                },
                error: (e) => {
                    console.log("getBasket error: " + e)
                    this.basket = new Basket("", this.currentUser!.id, [], "")
                },
            })
        } else {
            this.getLocaleBasket()
        }
    }

    private editBasket() {
        this.basket?.majBasket();
        if (this.authenticationService.userIsLogged()) {
            this.currentUser = AuthenticationService.currentUser
            this.basket!.customerId = this.currentUser!.id
            console.log("this.currentUser: ", this.currentUser)
            this.http.post(this.url + "basket/updateOne/" + this.currentUser!.id, this.basket).subscribe({
                next: (response: any) => {
                    //this.basket = response
                    //this.saveLocaleBasket()
                },
                error: (e) => {
                    console.log(e)
                },
            })
        }
        this.saveLocaleBasket()
    }

    public addProductToBasket(product: Product) {
        console.log("basket: " + this.basket)
        const alreadyExistingProductIndex = this.basket!.products.findIndex(p => p.id == product.id)
        if (alreadyExistingProductIndex == -1) {
            this.basket?.products.push(product)
        } else {
            this.basket!.products[alreadyExistingProductIndex].quantity++
        }
        this.editBasket()
    }

    public removeProductFromBasket(product: Product) {
        const alreadyExistingProductIndex = this.basket!.products.findIndex(p => p.id == product.id)
        if (alreadyExistingProductIndex > -1) {
            if (this.basket!.products[alreadyExistingProductIndex]!.quantity > 1) {
                this.basket!.products[alreadyExistingProductIndex]!.quantity--
            } else {
                this.basket!.products.splice(alreadyExistingProductIndex, 1)
            }
            this.editBasket()
        }
    }

    public getLocaleBasket() {
        const item = localStorage.getItem('basket')
        if (item) {
            this.basket = JSON.parse(item)
        } else {
            this.basket = new Basket("", "", [], "")
        }
    }

    public saveLocaleBasket() {
        localStorage.setItem('basket', JSON.stringify(this.basket))
    }


    public createCustomer(user: CustomerCRM) {
        if (this.authenticationService.userIsLogged()) {
            this.http.post(this.url + "customer/createOne", user).subscribe({
                error: (e) => {
                    console.log(e)
                },
            })
        }
    }

    public editCustomer(user: CustomerCRM) {
        if (this.authenticationService.userIsLogged()) {
            this.http.post(this.url + "customer/updateOne" + user.id, user).subscribe({
                error: (e) => {
                    console.log(e)
                },
            })
        }
    }

    public createOrder(idProduct: string, price: number) {
        const order = new OrderCRM(idProduct, price)
        order.customerId = this.currentUser?.id
        if (this.authenticationService.userIsLogged()) {
            this.http.post(this.url + "order/createOne", order).subscribe({
                error: (e) => {
                    console.log(e)
                },
            })
        }
    }

}
