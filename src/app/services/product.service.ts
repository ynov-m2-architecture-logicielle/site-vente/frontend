import { Injectable } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { environment } from "src/environments/environment"
import { Observable } from "rxjs"
import { Product } from "../models/product"
import { Tag, TagCategory } from "../models/tag"
@Injectable({
    providedIn: "root"
})
export class ProductService {
    url = environment.CONFIGURATEUR_OFFRE_BACKEND_URL
    //private url = 'http://localhost:8397/api/product-config';

    tags: Tag[] = [
        new Tag({ id: "0", name: "Chaises", category: TagCategory.TYPE, imageSrc: "https://www.ikea.com/global/assets/navigation/images/chairs-fu002.jpeg?imwidth=300" }),
        new Tag({ id: "1", name: "Tables", category: TagCategory.TYPE, imageSrc: "https://www.ikea.com/global/assets/navigation/images/tables-desks-fu004.jpeg?imwidth=300" }),
        new Tag({ id: "2", name: "Canapés", category: TagCategory.TYPE, imageSrc: "https://www.ikea.com/global/assets/navigation/images/sofas-fu003.jpeg?imwidth=300" }),
        new Tag({ id: "3", name: "Armoires", category: TagCategory.TYPE, imageSrc: "https://www.ikea.com/global/assets/navigation/images/wardrobes-19053.jpeg?imwidth=300" }),
        new Tag({ id: "4", name: "Commodes", category: TagCategory.TYPE, imageSrc: "https://www.ikea.com/global/assets/navigation/images/chests-of-drawers-drawer-units-st004.jpeg?imwidth=300" }),
        new Tag({ id: "5", name: "Lits", category: TagCategory.TYPE, imageSrc: "https://www.ikea.com/global/assets/navigation/images/beds-bm003.jpeg?imwidth=300" }),
        new Tag({ id: "6", name: "Fauteuils", category: TagCategory.TYPE, imageSrc: "https://www.ikea.com/global/assets/navigation/images/armchairs-chaise-longues-fu006.jpeg?imwidth=300" }),
        new Tag({ id: "7", name: "Rangements", category: TagCategory.TYPE, imageSrc: "https://www.ikea.com/fr/fr/navigation/images/rangements-bureau-et-salon-st003.jpeg?imwidth=300" }),
        new Tag({ id: "8", name: "Mobilier de jardin", category: TagCategory.TYPE, imageSrc: "https://www.ikea.com/global/assets/navigation/images/outdoor-furniture-od003.jpeg?imwidth=300" }),
    ]

    products: Product[] = [
        new Product({ id: "0", name: "Bergmund", price: 59.95, description: "Grâce à nos recherches et au développement de nouvelles techniques, la chaise vous offre un confort optimal. Pour changer son style, vous pouvez simplement choisir une housse d'une autre couleur ou d'un autre modèle.", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/bergmund-chaise-blanc-orrsta-gris-clair__0926649_pe789434_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
        new Product({ id: "1", name: "Nordmyra", price: 39.99, description: "Les chaises peuvent être empilées pour libérer de l'espace quand elles ne servent pas.", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/nordmyra-chaise-blanc-bouleau__0874347_pe649245_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
        new Product({ id: "2", name: "Kaettil", price: 39.99, description: "Cette chaise rembourrée avec dossier haut permet d'être installé confortablement autour de la table. Pour vous faciliter la vie, la housse lavable est faite d'une pièce et s'attache grâce à des bandes autoagrippantes.", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/kaettil-chaise-blanc-knisa-gris-clair__1028687_pe835486_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
        new Product({ id: "3", name: "Stefan", price: 19.99, description: "La structure en bois de cette chaise est particulièrement solide et peut résister aux assauts de la vie quotidienne.", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/stefan-chaise-brun-noir__0870436_pe672126_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
        new Product({ id: "4", name: "Lisabo", price: 29.95, description: "Une belle chaise", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/lisabo-chaise-frene__1079988_pe857792_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
        new Product({ id: "5", name: "Ingolf", price: 59.95, description: "Une belle chaise", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/ingolf-chaise-blanc__0209109_pe323885_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
        new Product({ id: "6", name: "Odger", price: 29.99, description: "Une belle chaise", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/odger-chaise-anthracite__0963408_ph170906_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
        new Product({ id: "7", name: "Adde", price: 49.99, description: "Une belle chaise", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/adde-chaise-blanc__0871875_pe716741_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
        new Product({ id: "8", name: "Sandsberg", price: 29.99, description: "Une belle chaise", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/sandsberg-chaise-noir-teinte-brun__1027685_pe834982_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
        new Product({ id: "9", name: "janinge", price: 19.99, description: "Une belle chaise", tags: [this.tags[0]], imageSrc: "https://www.ikea.com/fr/fr/images/products/janinge-chaise-blanc__0872443_pe653411_s5.jpg?f=xxs", quantity: 1, relatedProducts: [] }),
    ]

    constructor(private http: HttpClient) {
        this.products[0].relatedProducts = [this.products[1], this.products[2], this.products[3]]
        // this.getTags()
        // this.getProducts()
    }

    public getTags() {
        this.http.get(this.url + "/tags").subscribe({
            next: (response: any) => {
                this.tags = response
            },
            error: (e) => {
                console.log(e)
            },
        })
    }

    public getProducts() {
        this.http.get(this.url + "/products").subscribe({
            next: (response: any) => {
                this.products = response
            },
            error: (e) => {
                console.log(e)
            },
        })
    }
}
