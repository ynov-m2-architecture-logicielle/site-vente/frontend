import { Injectable } from "@angular/core"
import { HttpClient } from "@angular/common/http"
import { environment } from "src/environments/environment"
import { Stripe } from "stripe-angular"
import { Product } from "../models/product"
@Injectable({
    providedIn: "root"
})
export class ShopService {
    url = environment.SITE_VENTE_BACKEND_URL + "shop/"

    constructor(private http: HttpClient) { }

    public stripe = Stripe('pk_test_51ICMVxHmOoJeDnTyyTlROjI2TYpe5JPpmeweyoOKJectQZqQevrYawhcfzOvTDScfcU1vlGDLmj0z3N68lcclW6100TpCBQNpH')

    public createCheckoutSession(products: Product[]) {
        if (products.length > 0) {
            this.http.post(this.url + 'create-checkout-session', { products: products }).subscribe({
                next: (res: any) => {
                    return this.stripe.redirectToCheckout({ sessionId: res.id }).then((response: any) => {
                        console.log(response)
                    }).catch((error: any) => {
                        console.log(error)
                    })
                },
                error: (err: any) => {
                    console.log(err);

                },
            })
        }
    }
}
