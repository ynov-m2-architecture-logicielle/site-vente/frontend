import { HttpClient } from "@angular/common/http"
import { Injectable } from "@angular/core"
import * as CryptoJS from "crypto-js"
import { Observable } from "rxjs"
import { environment } from "src/environments/environment"
import { FrontendUser } from "../models/user/frontend-user"

@Injectable({
    providedIn: "root",
})
export class AuthenticationService {

    public static currentUser: FrontendUser | undefined

    public constructor(private http: HttpClient) {
        this.setCurrentUser()
    }

    private url: string = `${environment.SITE_VENTE_BACKEND_URL}auth/`

    public register(username: string, email: string, password: string): Observable<any> {
        return this.http.post(this.url + "register", { username, email, password: this.encryptData(password) })
    }

    public login(email: string, password: string): Observable<any> {
        return this.http.post(this.url + "login", { email, password: this.encryptData(password) })
    }

    public checkToken(): Observable<any> {
        return this.http.get(this.url + "check-token")
    }

    public checkAdminRight(): Observable<any> {
        return this.http.get(this.url + "check-admin-right")
    }

    public setCurrentUser() {
        const item = localStorage.getItem("currentUser")
        if (item) {
            const userInStorage: FrontendUser = JSON.parse(item)
            AuthenticationService.currentUser = userInStorage
            if (userInStorage && userInStorage.token) {
                // this.checkToken().subscribe({
                //     next: (response) => {
                //         AuthenticationService.currentUser!.token = response.token
                //         this.saveUserInLocalStorage(AuthenticationService.currentUser!)
                //         return
                //     },
                //     error: (e) => {
                //         console.log(e)
                //         this.logout()
                //     },
                // })
            }
        } else {
            console.log("do nothing")
            // this.saveUserInLocalStorage(AuthenticationService.currentUser!)
        }
    }

    public getUser() {
        return AuthenticationService.currentUser
    }

    public logout() {
        localStorage.removeItem("currentUser")
        AuthenticationService.currentUser = undefined
    }

    public saveUserInLocalStorage(user: FrontendUser) {
        AuthenticationService.currentUser = user
        localStorage.setItem("currentUser", JSON.stringify(user))
    }

    public userIsLogged(): boolean {
        if (!AuthenticationService.currentUser?.token || !AuthenticationService.currentUser?.id) {
            return false
        }
        return true
    }

    public encryptData(data: string): string {
        return CryptoJS.AES.encrypt(JSON.stringify(data), environment.CRYPTO_SECRET_KEY).toString()
    }
}

// import { Injectable } from "@angular/core"
// import {
//     HttpClient,
// } from "@angular/common/http"
// import { Observable } from "rxjs"
// import { environment } from "src/environments/environment"
// @Injectable({
//     providedIn: "root"
// })
// export class AuthenticationService {
//     url = environment.siteVenteBackendUrl + "auth/"

//     constructor(private http: HttpClient) { }

//     public register(username: string, password: string): Observable<any> {
//         return this.http.post(this.url + "register", { username, password })
//     }

//     public login(username: string, password: string): Observable<any> {
//         return this.http.post(this.url + "login", { username, password })
//     }

//     public checkToken(): Observable<any> {
//         return this.http.get(this.url + "check-token", { headers: { authorization: localStorage.getItem('token') || "" } })
//     }

//     isLogged() {
//         if (localStorage.getItem('token') && localStorage.getItem('username')) {
//             return true
//         }
//         return false
//     }

//     logout() {
//         localStorage.removeItem('token')
//         localStorage.removeItem('username')
//     }

//     getToken() { return localStorage.getItem('token') }
// }
