import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { HomeComponent } from './components/home/home.component'
import { CatalogComponent } from './components/catalog/catalog.component'
import { ShopComponent } from './components/shop/shop.component'
import { ProductComponent } from './components/product/product.component'
import { BasketComponent } from './components/basket/basket.component'


const routes: Routes = [
  { path: "panier", component: BasketComponent },
  { path: "c", component: CatalogComponent },
  { path: "c/:itemType", component: CatalogComponent },
  { path: "p/:productId", component: ProductComponent },
  { path: "shop/cancel", data: ["cancel"], component: ShopComponent },
  { path: "shop/success", data: ["success"], component: ShopComponent },
  // { path: "", component: HomeComponent },

  { path: "**", redirectTo: "c", pathMatch: "full" }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }