import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { HomeComponent } from './components/home/home.component'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { MatInputModule } from '@angular/material/input'
import { MatFormFieldModule } from '@angular/material/form-field'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatButtonModule } from '@angular/material/button'
import { HeaderComponent } from './components/header/header.component'
import { ShopComponent } from './components/shop/shop.component'
import { RegisterDialogComponent } from './components/register-dialog/register.dialog.component'
import { MatCardModule } from "@angular/material/card"
import { MatCheckboxModule } from "@angular/material/checkbox"
import { MatExpansionModule } from "@angular/material/expansion"
import { MatSelectModule } from "@angular/material/select"
import { MatSlideToggleModule } from "@angular/material/slide-toggle"
import { MatTooltipModule } from "@angular/material/tooltip"
import { MatIconModule } from "@angular/material/icon";
import { MatRadioModule } from "@angular/material/radio";
import { MatSliderModule } from "@angular/material/slider";
import { MatDialogModule } from '@angular/material/dialog'
import { CatalogComponent } from './components/catalog/catalog.component'
import { BasketComponent } from './components/basket/basket.component'
import { ProductComponent } from './components/product/product.component'
import { JwtInterceptor } from './interceptors/jwt.interceptor'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CatalogComponent,
    BasketComponent,
    ProductComponent,
    ShopComponent,
    HeaderComponent,
    RegisterDialogComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatTooltipModule,
    MatSelectModule,
    MatCardModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatSliderModule,
    MatRadioModule,
    MatSlideToggleModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }, RegisterDialogComponent],
  bootstrap: [AppComponent],
  entryComponents: [RegisterDialogComponent]
})
export class AppModule { }
